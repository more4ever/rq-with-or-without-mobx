import React from 'react';
import {observer} from "mobx-react-lite";

const EventItem = ({event}) => {
    console.log('EventItem triggered', event.id)

    return (
        <div>
            {event.id}: {event.name}
        </div>
    );
}

export const EventItemMobx = observer(EventItem);

export default React.memo(EventItem);