import EventsRepoStore from "../stores/EventsRepoStore";
import {action, computed, makeObservable, observable} from "mobx";
import EventsRepo from "../stores/EventsRepo";


class Event {
    raw;

    constructor(raw) {
        this.raw = raw;

        makeObservable(this, {
            raw: observable,
            id: computed,
            name: computed,
            assign: action.bound,
        })
    }

    get id(){
        return this.raw.id;
    }

    get name(){
        return this.raw.name;
    }

    assign(newRaw){
        this.raw = newRaw;
    }
}

export const createStores = (params) => {
    const { api } = params;
    return {
        eventsStore: new EventsRepoStore(api.eventsApi, (rawEvent) => new Event(rawEvent)),
        eventsStoreProxy: new EventsRepo(api.eventsApi),
    };
};