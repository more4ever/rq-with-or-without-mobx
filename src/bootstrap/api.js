import EventsApi from "../apiProviders/EventsApi";


export const createApi = (apiBaseUri) => {
	return {
		eventsApi: new EventsApi(apiBaseUri),
	};
};

