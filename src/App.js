import {ApiProvider} from "./contexts/ApiProvider";
import {StoresProvider} from "./contexts/StoresProvider";
import {createApi} from "./bootstrap/api";
import {createStores} from "./bootstrap/stores";
import TestPage from "./pages/TestPageWithStorage";
import {QueryClient, QueryClientProvider} from "react-query";
import {BrowserRouter, Link, Route, Switch} from "react-router-dom";
import TestPage1 from "./pages/TestCache";
import TestPageWithoutStorage from "./pages/TestPageWithoutStorage";

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnMount: false,
            refetchOnWindowFocus: false,
        }
    }
});

function App() {
    return (
        <QueryClientProvider client={queryClient}>
            <ApiProvider apiFactory={() => createApi('/',)}>
                <StoresProvider storesFactory={createStores}>
                    <BrowserRouter>
                        <div>
                            <Link to='/'>Page with storage</Link> | <Link to='/test-without-storage'>Page without storage</Link> | <Link to='/test1'>Test Cache</Link>
                        </div>
                        <Switch>
                            <Route exact path='/'>
                                <TestPage/>
                            </Route>
                            <Route exact path='/test1'>
                                <TestPage1/>
                            </Route>
                            <Route exact path='/test-without-storage'>
                                <TestPageWithoutStorage/>
                            </Route>
                        </Switch>
                    </BrowserRouter>
                </StoresProvider>
            </ApiProvider>
        </QueryClientProvider>
    );
}

export default App;
