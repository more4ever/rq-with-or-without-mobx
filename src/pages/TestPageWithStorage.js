import React, {useCallback} from 'react';
import {observer} from "mobx-react-lite";


import useEventsListQuery from "../queryHooks/events/withStorage/useEventsListQuery";
import useEventsSingleQuery from "../queryHooks/events/withStorage/useEventsSingleQuery";
import useEventsCreateMutation from "../queryHooks/events/withStorage/useEventsCreateMutation";
import {EventItemMobx} from "../components/EventItem";
import useEventsUpdateMutation from "../queryHooks/events/withStorage/useEventsUpdateMutation";


const TestPageWithStorage = () => {
    const query = useEventsListQuery();
    const singleQuery = useEventsSingleQuery(2);

    const {mutate} = useEventsCreateMutation();
    const {mutate: updateEvent} = useEventsUpdateMutation();

    const handleCreateClick = useCallback(() => {
        mutate({name: 'testNew'})
    }, [mutate])

    const handleUpdateClick = useCallback(() => {
        updateEvent({id: 2, data: {name: 'test_updated'}})
    }, [updateEvent])

    const handleUpdateUnusedClick = useCallback(() => {
        updateEvent({id: 2, data: {unused: 'test_updated'}})
    }, [updateEvent])

    console.log('TestPageWithStorage updated')

    return (
        <>
            <h1>
                Page With Storage Provider
            </h1>
            <h2>
                list
            </h2>
            {query.status === 'success' && query.data.map((item) => <EventItemMobx key={item.id} event={item}/>)}

            <h2>
                single
            </h2>
            {singleQuery.status === 'success' && <EventItemMobx event={singleQuery.data}/>}
            <br/>
            <div>
                <button onClick={handleCreateClick}>
                    create
                </button>
                <button onClick={handleUpdateClick}>
                    update #2 name
                </button>
                <button onClick={handleUpdateUnusedClick}>
                    update #2 unused field
                </button>
            </div>
        </>
    );
}

export default observer(TestPageWithStorage);