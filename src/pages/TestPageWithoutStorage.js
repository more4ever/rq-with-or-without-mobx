import React, {useCallback} from 'react';

import EventItem from "../components/EventItem";
import useEventsListQueryApi from "../queryHooks/events/withoutStorage/useEventsListQueryApi";
import useEventsSingleQueryApi from "../queryHooks/events/withoutStorage/useEventsSingleQueryApi";
import useEventsCreateMutationApi from "../queryHooks/events/withoutStorage/useEventsCreateMutationApi";
import useEventsUpdateMutationApi from "../queryHooks/events/withoutStorage/useEventsUpdateMutationApi";


const TestPageWithoutStorage = () => {
    const query = useEventsListQueryApi();
    const singleQuery = useEventsSingleQueryApi(2);

    const {mutate} = useEventsCreateMutationApi();
    const {mutate: updateEvent} = useEventsUpdateMutationApi();

    const handleCreateClick = useCallback(() => {
        mutate({name: 'testNew'})
    }, [mutate])

    const handleUpdateClick = useCallback(() => {
        updateEvent({id: 2, data: {name: 'test_updated'}})
    }, [updateEvent])

    const handleUpdateUnusedClick = useCallback(() => {
        updateEvent({id: 2, data: {unused: 'test_updated'}})
    }, [updateEvent])

    console.log('TestPageWithoutStorage updated');

    return (
        <>

            <h1>
                Page Without Storage Provider
            </h1>
            <h2>
                list
            </h2>
            {query.status === 'success' && query.data.map((item) => <EventItem key={item.id} event={item}/>)}

            <h2>
                single
            </h2>
            {singleQuery.status === 'success' && <EventItem event={singleQuery.data}/>}
            <br/>
            <div>
                <button onClick={handleCreateClick}>
                    create
                </button>
                <button onClick={handleUpdateClick}>
                    update #2 name
                </button>

                <button onClick={handleUpdateUnusedClick}>
                    update #2 unused field
                </button>
            </div>
        </>
    );
}

export default TestPageWithoutStorage;