const TestPage = () => {

    return 'When you go back to previous page, cached data will be displayed without any additional requests';
}

export default TestPage;