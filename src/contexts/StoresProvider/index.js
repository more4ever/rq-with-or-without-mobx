import React, { useContext, useEffect, useState } from 'react';

import { useApi } from '../ApiProvider';

export const storesContext = React.createContext(null);

export function StoresProvider(props) {
	const { children, storesFactory } = props;

	const [stores, setStores] = useState(null);
	const api = useApi();

	useEffect(() => {
		setStores(storesFactory({ api }));
	}, [api, storesFactory]);

	if (!stores) {
		return null;
	}

	// @ts-ignore
	window.s = stores;

	return <storesContext.Provider value={stores}>{children}</storesContext.Provider>;
}

export const useStores = () => useContext(storesContext);
