import React, { useContext, useEffect, useState } from 'react';


export const apiContext = React.createContext(null);

export function ApiProvider(props) {
	const { children, apiFactory } = props;
	const [api, setApi] = useState(null);

	useEffect(() => {
		setApi(apiFactory());
	}, [apiFactory]);

	if (!api) {
		return null;
	}

	return <apiContext.Provider value={api}>{children}</apiContext.Provider>;
}

export const useApi = () => useContext(apiContext);
