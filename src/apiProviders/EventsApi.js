import axios from "axios";


const mockEvents = [
	{id: 1, name: 'test1'},
	{id: 2, name: 'test2'},
	{id: 3, name: 'test3'},
	{id: 4, name: 'test4'},
	{id: 5, name: 'test5'},
]

let nextId = 6;

export default class EventsApi {
	client;

	constructor(baseUrl) {
		this.client = axios.create({
			baseURL: baseUrl,
			timeout: 20000,
		});

	}

	loadList(){
		const response = JSON.parse(JSON.stringify(mockEvents))

		console.log('list request', response);

		return Promise.resolve(response);
	}

	loadById(id){
		const item = mockEvents.find((item) => item.id === Number.parseInt(id));

		console.log('loadById request', item)

		return item ? Promise.resolve({...item}) : Promise.reject('Not Found');
	}

	create(newEvent){

		const newEventDoc = {...newEvent, id: nextId++};

		mockEvents.push(newEventDoc);

		console.log('create request', newEventDoc)

		return Promise.resolve(newEventDoc)
	}

	update(id, data){

		const eventForUpdate = mockEvents.find((event) => event.id === id);

		if(!eventForUpdate){
			return Promise.reject('Not found');
		}

		Object.assign(eventForUpdate, data, {id})

		console.log('update request', eventForUpdate)

		return Promise.resolve(eventForUpdate)
	}
}
