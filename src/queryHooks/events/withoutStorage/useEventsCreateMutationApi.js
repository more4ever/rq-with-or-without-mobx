import {useMutation, useQueryClient} from "react-query";

import {eventsListQueryApiKeyPrefix} from "./useEventsListQueryApi";
import {useStores} from "../../../contexts/StoresProvider";



const useEventsCreateMutationApi = () => {
	const { eventsStoreProxy } = useStores();

	const queryClient = useQueryClient();

	return useMutation(
		(newEvent) => {
			console.log('useEventsCreateMutationApi');
			return eventsStoreProxy.createNew(newEvent)
		},
		{
			onSuccess: () => {
				console.log('useEventsCreateMutationApi invalidate: ', eventsListQueryApiKeyPrefix)

				queryClient.invalidateQueries(eventsListQueryApiKeyPrefix);
			},
		}
	);
};

export default useEventsCreateMutationApi;
