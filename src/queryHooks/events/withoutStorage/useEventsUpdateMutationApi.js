import {useMutation, useQueryClient} from "react-query";

import {eventsListQueryApiKeyPrefix} from "./useEventsListQueryApi";
import {eventsSingleQueryApiKey} from "./useEventsSingleQueryApi";
import {useStores} from "../../../contexts/StoresProvider";


const useEventsUpdateMutationApi = () => {
	const { eventsStoreProxy } = useStores();

	const queryClient = useQueryClient();

	return useMutation(
		({id, data}) => {
			console.log('useEventsUpdateMutationApi')
			return eventsStoreProxy.update(id, data)
		},
		{
			onSuccess: (data) => {
				console.log('useEventsUpdateMutationApi invalidate', eventsSingleQueryApiKey(data.id), eventsListQueryApiKeyPrefix)

				queryClient.invalidateQueries(eventsSingleQueryApiKey(data.id));

				queryClient.invalidateQueries(eventsListQueryApiKeyPrefix);
			},
		}
	);
};

export default useEventsUpdateMutationApi;
