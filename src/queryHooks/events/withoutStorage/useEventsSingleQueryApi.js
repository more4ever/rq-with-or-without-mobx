import { useQuery } from 'react-query';
import {useStores} from "../../../contexts/StoresProvider";


export const eventsSingleQueryApiKeyPrefix = 'eventsSingleApi';
export const eventsSingleQueryApiKey = (id) => [
	eventsSingleQueryApiKeyPrefix,
	id
];

const useEventsSingleQueryApi = (id) => {
	const { eventsStoreProxy } = useStores();

	return useQuery(eventsSingleQueryApiKey(id), async (...args) => {

		const response = await eventsStoreProxy.loadSingle(id)
		console.log('useEventsSingleQueryApi', response);
		return response;
	});
};

export default useEventsSingleQueryApi;
