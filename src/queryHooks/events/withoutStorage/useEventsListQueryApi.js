import { useQuery } from 'react-query';
import {useStores} from "../../../contexts/StoresProvider";


export const eventsListQueryApiKeyPrefix = 'eventsListApi';
export const eventsListQueryApiKey = (isParticipating = undefined, statuses = [], userId = undefined) => [
	eventsListQueryApiKeyPrefix,
	isParticipating,
	statuses,
	userId,
];

const useEventsListQueryApi = (isParticipating = undefined, statuses = [], userId = undefined) => {

	const { eventsStoreProxy } = useStores();

	return useQuery(eventsListQueryApiKey(isParticipating, statuses, userId), async (...args) => {
		const list =  await eventsStoreProxy.loadList(isParticipating, statuses, userId);
		console.log('useEventsListQueryApi response', list)
		return list;
	});
};

export default useEventsListQueryApi;
