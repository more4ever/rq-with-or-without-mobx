import { useQuery } from 'react-query';
import {useStores} from "../../../contexts/StoresProvider";


export const eventsListQueryKeyPrefix = 'eventsList';
export const eventsListQueryKey = (isParticipating = undefined, statuses = [], userId = undefined) => [
	eventsListQueryKeyPrefix,
	isParticipating,
	statuses,
	userId,
];

const useEventsListQuery = (isParticipating = undefined, statuses = [], userId = undefined) => {
	const { eventsStore } = useStores();

	return useQuery(eventsListQueryKey(isParticipating, statuses, userId), async (...args) => {
		const list =  await eventsStore.loadList(isParticipating, statuses, userId);
		console.log('useEventsListQuery response', list)
		return list;
	});
};

export default useEventsListQuery;
