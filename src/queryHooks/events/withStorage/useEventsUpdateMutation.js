import {useMutation} from "react-query";

import {useStores} from "../../../contexts/StoresProvider";


const useEventsUpdateMutation = () => {
	const { eventsStore } = useStores();

	return useMutation(
		({id, data}) => {
			console.log('useEventsUpdateMutation')
			return eventsStore.update(id, data)
		},
	);
};

export default useEventsUpdateMutation;
