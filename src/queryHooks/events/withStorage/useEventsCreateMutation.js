import {useMutation, useQueryClient} from "react-query";

import {useStores} from "../../../contexts/StoresProvider";
import {eventsListQueryKeyPrefix} from "./useEventsListQuery";


const useEventsCreateMutation = () => {
	const { eventsStore } = useStores();
	const queryClient = useQueryClient();

	return useMutation(
		(newEvent) => {
			console.log('useEventsCreateMutation')

			return eventsStore.createNew(newEvent)
		},
		{
			onSuccess: () => {
				console.log('useEventsCreateMutation invalidate: ', eventsListQueryKeyPrefix)
				queryClient.invalidateQueries(eventsListQueryKeyPrefix);
			},
		}
	);
};

export default useEventsCreateMutation;
