import { useQuery } from 'react-query';
import {useStores} from "../../../contexts/StoresProvider";


export const eventsSingleQueryKeyPrefix = 'eventsSingle';
export const eventsSingleQueryKey = (id) => [
	eventsSingleQueryKeyPrefix,
	id
];

const useEventsSingleQuery = (id) => {
	const { eventsStore } = useStores();

	return useQuery(eventsSingleQueryKey(id), (...args) => {
		console.log('useEventsSingleQuery')
		return eventsStore.loadSingle(id);
	});
};

export default useEventsSingleQuery;
