import {observable, computed, action, makeObservable} from 'mobx';
import keyBy from 'lodash/keyBy';

export default class RepoStore {
	entityFactory;

	entities = [];

	constructor(
		entityFactory = (rawEntity) => rawEntity
	) {
		this.entityFactory = entityFactory;

		makeObservable(this, {
			remove: action.bound,
			create: action.bound,
			clearAllCache: action.bound,
			entitiesById: computed,
			entities: observable,
		})
	}

	get entitiesById() {
		return keyBy(this.entities, 'id');
	}

	getById(id) {
		return this.entitiesById[id];
	}

	getByIds(ids) {
		return ids.map((id) => this.getById(id)).filter((entity) => !!entity);
	}

	// this can't be an action or it will create issues with auto dirty flagging
	addOverrideEntities(rawEntities) {
		return rawEntities.map((entityRaw) => this.addOverrideEntity(entityRaw));
	}

	// this can't be an action or it will create issues with auto dirty flagging
	addOverrideEntity(rawEntity) {
		let entity = this.getById(rawEntity.id);

		if (entity) {
			entity.assign(rawEntity);
		} else {
			entity = this.create(rawEntity);
		}
		return entity;
	}


	remove(entity) {
		this.entities.remove(entity);
	}


	create(rawEntity) {
		const entity = this.entityFactory(rawEntity, this);
		this.entities.push(entity);
		return entity;
	}

	clearAllCache() {
		this.entities.clear();
	}
}
