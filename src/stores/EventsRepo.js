import BaseRepo from "./BaseRepo";

export default class EventsRepo extends BaseRepo{
	 api;

	constructor(api) {
		super();
		this.api = api;
	}

	loadList(
		isParticipating = false,
		statuses = [],
		userId = undefined
	) {
		return this.api.loadList(isParticipating, statuses, userId);
	}

	loadSingle(id) {
		return this.api.loadById(id);
	}

	createNew(eventData){
		return this.api.create(eventData)
	}

	update(id, data){
		return this.api.update(id, data)

	}
}
