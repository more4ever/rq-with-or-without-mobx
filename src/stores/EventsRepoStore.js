import RepoStore from "./RepoStore";

export default class EventsRepoStore extends RepoStore {
	 api;

	constructor(api, entityFactory) {
		super(entityFactory);

		this.api = api;
	}

	loadList(
		isParticipating = false,
		statuses = [],
		userId = undefined
	) {
		return this.api.loadList(isParticipating, statuses, userId).then((response) => this.addOverrideEntities(response));
	}

	loadSingle(id) {
		return this.api.loadById(id).then((response) => this.addOverrideEntity(response));
	}

	createNew(eventData){
		return this.api.create(eventData).then((response) => this.addOverrideEntity(response))
	}

	update(id, data){
		return this.api.update(id, data).then((response) => this.addOverrideEntity(response))

	}
}
