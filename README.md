# Demo of React-query (RQ) package usage with and without storage layer

### Setup

```shell
yarn install --frozen-lockfile
yarn start
```

### Project UI structure

- Pages
  - Page with storage - represent behaviour of RQ + Mobx storage
  - Page without storage - represent behaviour of pure RQ implementation
  - Test cache - for testing RQ cache mechanism
- Buttons
  - add new entity
  - update entity property which used during render
  - update entity property which unused during render
- console log represent callstack

### Project code structure
- RQ init with: ```refetchOnMount: false, refetchOnWindowFocus: false```

## Some points related to RQ itself independently of approach

Pros:
- full control over query lifecycle
- edge cases queries (Pagination, infinity queries, observer queries, etc.)
- easy for use interface and great documentation
- Typescript support 
- data invalidation process - any magic all in your code
- build in deep compare of current data with received from re-fetch - possible to keep reference during re-fetch

Cons:
- data invalidation process - you have a chance to forget add query to invalidation hook. Side effects global manager advised by RQ
- native react rerender logic, if property reference changed, rerender will be triggered

## About

This repository represents two different approaches of RQ usage.

### Usage of RQ without mobx and additional data storage provider. Data storage handled by RQ:

query chain
```
Component -> useQueryCustomHook -> useQueryHook -> EntityRepo (as proxy layer, additional encapsulation) -> Entity Data Provider (Api)
```
   
Pros:
- simple structure
- no additional logic for data storage
- no additional logic for response storages
- easy control of GC 
     
Cons:
- de-normalised data - each query cache doesn't check existing data and can contain different version or duplications of the same entity

Uncategorized:
- for prevent item rerender it should be wrapped with React.memo for case when list reference was changed but item reference was saved.
More about React mechanism

### Usage of RQ with mobx as data storage layer (inspired by [Mateo post](https://dev.to/lloyds-digital/normalize-your-react-query-data-with-mobx-state-tree-17fa) ):

Pros:
- trigger component rerender only if used variable was changed (example of unused item property update)
- normalised data - mobx storages keep all data in one place which give ability to keep single version of entity.
RQ receive references to unique item
- update queries can omit cache invalidations (mobx magic on entity level) - only if new entity response structure always the same

Cons:
- additional logic for storage
- GC issue - we keep reference to entity in two places (RQ storage and MobX storage)
- additional complexity of the system
- mobx magic applied to component renders


### Suggestion

Start working from base version of (RQ -> Repo (as proxy layer) ->API)
- encapsulation of RQ logic from API - Repository is a proxy  between cache and API
- in any time we can integrate normalisation layer on repository level (in case of memory leeks, etc.)